// TODO: MANY THINGS




// GOAL FOR REDESIGN
- Layering? 
    - ability to move to front, move to back, move up down by 1
- More global handlers
- Alternate way of handling ownership of objects
    - Maybe it is done where all objects are on the same level. (ala powerpoint)
    - we allow grouping of objects
    - File format considerations
- Different Markdown Parsing (Modular)
    - Maybe even real time parsing e.g hiding the markdown entirely
- Atomic actions, allow for undo and redo i.e action history
- Clean MVC split
