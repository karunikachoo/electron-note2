/** Acts as a general wrapper for a bunch of handlers. */

import {PaperMouseHandler, PaperMouseParent} from "../input/paperMouseHandler";
import {PaperObjectHandler, PaperObjectParentInterface} from "./paperObjectHandler";
import {FrameListener} from "../draw/frameHandler";
import {SurfaceObject} from "../surface/surfaceObject";
import {PaperObject} from "./paperObject";
import {PaperBaseObject} from "./paperBaseObject";


export class Paper implements FrameListener, PaperMouseParent, PaperObjectParentInterface {

    id: string;
    c: HTMLCanvasElement;

    mouseHandler: PaperMouseHandler;
    objectHandler: PaperObjectHandler;

    // Frame Listener
    frameInterval: number;
    frameEvent: () => void;

    // Infinite Surface
    offset: number[] = [];
    scale: number = 1;
    fidel: number = 2;

    constructor(id: string) {
        this.id = id;
        this.c = document.getElementById(this.id) as HTMLCanvasElement;

        this.mouseHandler = new PaperMouseHandler(this);
        this.objectHandler = new PaperObjectHandler(this);
    }

    getCanvas(): HTMLCanvasElement {
        return this.c;
    }

    _limitX(x: number) { return x < 0 ? 0 : (x > this.c.width ? this.c.width : x); }
    _limitY(y: number) { return y < 0 ? 0 : (y > this.c.height ? this.c.height : y); }
    _isVisible(x0: number, y0: number, x1: number, y1: number) {
        x0 = this._limitX(x0);
        x1 = this._limitX(x1);
        y0 = this._limitY(y0);
        y1 = this._limitY(y1);

        return (x1 - x0) * (y1 - y0) > 0;
    }

    _drawCheck(o: PaperBaseObject) {
        return true;
    }


    draw(ctx: CanvasRenderingContext2D, ox: number, oy: number, s: number, f: number) {
        this.objectHandler.getOrder().forEach(id => {
            const o = this.objectHandler.get(id);
            if (o) {
                if (this._drawCheck(o)) {
                    o.draw(ctx, ox, oy, s, f);
                }
            }
        })
    }

    render() {
        if (!this.c) return;

        const ctx = this.c.getContext('2d');
        ctx.clearRect(0, 0, this.c.width, this.c.height);

        this.draw(ctx, this.offset[0], this.offset[1], this.scale, this.fidel);
    }

    onFrameUpdate(t: number, dt: number): void {
        this.render();
    }
}