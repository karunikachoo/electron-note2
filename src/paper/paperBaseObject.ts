/** Contains base logic for tracking object layer position and childing */
import {Utils} from "../utils/Utils";
import {POFlag, POLayer} from "./paperObjectHandler";
import {Paper} from "./paper";

export class PaperBaseObject {
    paper: Paper;

    id: string;
    rect: number[] = [0, 0, 0, 0];

    props: number = 0x0;
    layerNumber: number = POLayer.BASE;

    children: string[] = [];

    constructor(paper: Paper, id?:string) {
        this.id = id ? id : Utils.genId(10);
        this.paper = paper;
        this.paper.objectHandler.add(this);
    }

    setLayer(layer: POLayer) {
        this.layerNumber = layer;
        this.paper.objectHandler.add(this);
    }

    /** Property Flags */
    is(mask: POFlag): boolean { return Boolean(this.props & mask); }
    set(mask: POFlag) { this.props |= mask; }
    clear(mask: POFlag) { this.props &= ~mask; }
    toggle(mask: POFlag) { this.props ^= mask; }

    draw(ctx: CanvasRenderingContext2D, ox: number, oy: number, s: number, f: number) {
        this.children.forEach(id => {
            const o = this.paper.objectHandler.get(id);
            if (this.paper._drawCheck(o)) o.draw(ctx, ox, oy, s, f);
        })
    }
}