import {PaperObject} from "./paperObject";
import {Utils} from "../utils/Utils";
import {PaperBaseObject} from "./paperBaseObject";

export interface PaperObjectParentInterface {
    getCanvas(): HTMLCanvasElement;
}

export enum POLayer {
    NO_LAYER = -1 , // CHILD and NO_LAYER are not added to layers.
    UNDERLAY,
    BASE,
    OVERLAY = 99,
}

export enum POFlag {

}

export class PaperObjectHandler {
    parent: PaperObjectParentInterface;

    objects: Map<string, PaperBaseObject> = new Map<string, PaperBaseObject>();

    layers: Map<number, string[]> = new Map();
    layerKeys: number[] = [];

    constructor(parent: PaperObjectParentInterface) {
        this.parent = parent;
    }

    get(id: string): PaperBaseObject {
        return this.objects.get(id);
    }

    add(obj: PaperBaseObject) {
        this.objects.set(obj.id, obj);

        const lno = obj.layerNumber;
        this._removeFromOtherLayers(obj.id);

        if (lno > POLayer.NO_LAYER) {
            if (this.layers.has(lno)) {
                if (!this.layers.get(lno).includes(obj.id))
                    this.layers.get(lno).push(obj.id);
            } else {
                this.layers.set(lno, [obj.id]);
                this._updateLayerKeys();
            }
        }
    }

    remove(id: string) {
        if (this.objects.has(id)) {
            const obj = this.objects.get(id);

            this._removeFromOtherLayers(id);

            this.objects.delete(id);
            return obj;
        }
        return null;
    }

    /** Layering and Ordering */
    _removeFromOtherLayers(id: string) {
        this.layerKeys.forEach(layerId => {
            const i = this.layers.get(layerId).indexOf(id);
            if (i >= 0) this.layers.get(layerId).splice(i, 1);
        })
    }

    _updateLayerKeys() {
        this.layerKeys = Array.from(this.layers.keys());
        this.layerKeys.sort((a, b) => { return a - b; });
    }

    getOrder(): string[] {
        const ret: string[] = [];

        this.layerKeys.forEach(key => {
            ret.push(...this.layers.get(key));
        })

        return ret;
    }

    getObjectOrder(id: string): number {
        return this.layers.get(this.get(id).layerNumber).indexOf(id);
    }

    // order shifting
    shiftUp(id: string) {
        const lno = this.get(id).layerNumber;
        const order = this.layers.get(lno);
        const i = order.indexOf(id);

        if (i >= 0) {
            this.layers.set(lno, Utils.move(order, i, i + 1));
        }
    }
    shiftDown(id: string) {
        const lno = this.get(id).layerNumber
        const order = this.layers.get(lno);
        const i = order.indexOf(id);

        if (i >= 0)
            this.layers.set(lno, Utils.move(order, i, i - 1));
    }
    shiftTop(id: string) {
        const lno = this.get(id).layerNumber
        const order = this.layers.get(lno);
        const i = order.indexOf(id);

        if (i >= 0)
            this.layers.set(lno, Utils.move(order, i, order.length - 1));
    }
    shiftBottom(id: string) {
        const lno = this.get(id).layerNumber
        const order = this.layers.get(lno);
        const i = order.indexOf(id);

        if (i >= 0)
            this.layers.set(lno, Utils.move(order, i, 0));
    }

}