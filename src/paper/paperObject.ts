import {PaperBaseObject} from "./paperBaseObject";


export class PaperObject extends PaperBaseObject {
    parent: string;

    children: string[] = [];


    addChild(o: PaperObject) {
        if (!this.children.includes(o.id)) this.children.push(o.id);
    }


    draw(ctx: CanvasRenderingContext2D, ox: number, oy: number, s: number, f: number) {

    }

}
