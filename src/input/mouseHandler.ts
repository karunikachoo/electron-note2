import {Surface} from "../surface/surface";
import Timeout = NodeJS.Timeout;


export interface MouseInputListener {
    id: string;
    rect: number[];

    onClick(mId:number, xy:number[], evt:MouseEvent): void;
    onDoubleClick(mId:number, xy:number[], evt:MouseEvent): void;
    onDrag(mId:number, xy:number[], dxy:number[], evt:MouseEvent): void;
    // onDragMouseUp(mId:number, xy:number[], evt:MouseEvent): void;
    onHover(xy:number[], evt:MouseEvent): void;
    onMouseEnter(xy:number[], evt:MouseEvent): void;
    onMouseExit(xy:number[], evt:MouseEvent): void;
    onScroll(dxy:number[], evt:WheelEvent): void;
    onZoom(dy:number, xy:number[], evt:WheelEvent): void;
}


export class MouseHandler {
    parent: Surface;

    // listeners: MouseInputListener[] = [];
    listeners: Map<string, MouseInputListener> = new Map<string, MouseInputListener>();

    constructor(parent: Surface) {
        this.parent = parent;

        this.onMouseDown = this.onMouseDown.bind(this);
        this.onMouseUp = this.onMouseUp.bind(this);
        this.onMouseMove = this.onMouseMove.bind(this);
        this.onDoubleClick = this.onDoubleClick.bind(this);
        this.onWheel = this.onWheel.bind(this);
    }

    init() {
        this.parent.c.onmousedown = this.onMouseDown;
        this.parent.c.onmouseup = this.onMouseUp;
        this.parent.c.onmousemove = this.onMouseMove;
        this.parent.c.ondblclick = this.onDoubleClick;
        this.parent.c.onwheel = this.onWheel;
    }

    register(listener: MouseInputListener) {
        if (!this.listeners.has(listener.id)) this.listeners.set(listener.id, listener);
    }

    remove(listener: MouseInputListener) {
        if (this.listeners.has(listener.id)) this.listeners.delete(listener.id);
    }

    /** Get bounding client rect for canvas and use that offset when calc
     *  mouse xy
     */

    _notifyClick(evt:MouseEvent) {
        const order = [...this.parent.getOrder()].reverse();

        for (let i = 0; i < order.length; i++) {
            if (this.listeners.has(order[i])) {
                const listener = this.listeners.get(order[i]);
                const [x, y] = this._computeRelative(evt.pageX, evt.pageY);
                if (this._isWithin(x, y, listener)) {
                    listener.onClick(evt.button, [x, y], evt);
                    break;
                }
            }
        }
    }

    isDown = false;
    isMoved = false;
    mxy: number[] = [0, 0];
    onMouseDown(evt: MouseEvent) {
        this.mxy = [evt.pageX, evt.pageY]
        this.isDown = true;
    }

    onMouseUp(evt: MouseEvent) {
        this.isDown = false;
        if (!this.isMoved) {
            this._notifyClick(evt);
        } else {
            this.dragTarget = null;
        }
    }

    onMouseMove(evt: MouseEvent) {
        if (this.isDown) {
            this.isMoved = true;
            const dxy = [evt.pageX - this.mxy[0], evt.pageY - this.mxy[1]];
            this.mxy = [evt.pageX, evt.pageY]
            this._notifyDrag(dxy, evt);
        } else {
            this._notifyHover(evt);
        }
    }

    hoverTarget: string = null;
    _notifyHover(evt:MouseEvent) {
        const order = [...this.parent.getOrder()].reverse();

        for (let i = 0; i < order.length; i++) {
            const id = order[i];
            if (this.listeners.has(id)) {
                const listener = this.listeners.get(id);
                const [x, y] = this._computeRelative(evt.pageX, evt.pageY);
                if (this._isWithin(x, y, listener)) {
                    if (this.hoverTarget !== listener.id) {
                        if (this.hoverTarget)
                            this.listeners.get(this.hoverTarget).onMouseExit([x, y], evt);
                        listener.onMouseEnter([x, y], evt);
                        this.hoverTarget = listener.id;
                    }
                    listener.onHover([x, y], evt);
                    break;
                }
            }
        }
    }

    dragTarget: string = null;
    _notifyDrag(dxy:number[], evt:MouseEvent) {
        if (this.dragTarget) {
            const [x, y] = this._computeRelative(evt.pageX, evt.pageY);
            if (this.listeners.has(this.dragTarget)) {
                this.listeners.get(this.dragTarget).onDrag(evt.button, [x, y], dxy, evt);
            }
        } else {
            const order = [...this.parent.getOrder()].reverse();

            for (let i = 0; i < order.length; i++) {
                if (this.listeners.has(order[i])) {
                    const listener = this.listeners.get(order[i]);
                    const [x, y] = this._computeRelative(evt.pageX, evt.pageY);
                    if (this._isWithin(x, y, listener)) {
                        this.dragTarget = listener.id;
                        listener.onDrag(evt.button, [x, y], dxy, evt);
                        break;
                    }
                }
            }
        }
    }

    onDoubleClick(evt: MouseEvent) {
        const order = [...this.parent.getOrder()].reverse();

        for (let i = 0; i < order.length; i++) {
            if (this.listeners.has(order[i])) {
                const listener = this.listeners.get(order[i]);
                const [x, y] = this._computeRelative(evt.pageX, evt.pageY);
                if (this._isWithin(x, y, listener)) {
                    listener.onDoubleClick(evt.button, [x, y], evt);
                    break;
                }
            }
        }
    }

    wheelDelay = 50;  // ms
    wheelTarget:string = null;
    wheelTimeout: Timeout;
    _checkWheel(id: string, evt: WheelEvent) {
        this.wheelTarget = id;
        clearTimeout(this.wheelTimeout);
        this.wheelTimeout = setTimeout(() => {this.wheelTarget = null;}, this.wheelDelay);
        return [-evt.deltaX * 0.5, -evt.deltaY * 0.5];
    }

    onWheel(evt: WheelEvent) {
        if (evt.ctrlKey) {
            this._notifyZoom(evt);
        } else {
            this._notifyScroll(evt);
        }
    }

    _notifyScroll(evt:WheelEvent) {
        const order = [...this.parent.getOrder()].reverse();
        if (!this.wheelTarget) {
            for (let i = 0; i < order.length; i++) {
                if (this.listeners.has(order[i])) {
                    const listener = this.listeners.get(order[i]);
                    const [x, y] = this._computeRelative(evt.pageX, evt.pageY);
                    if (this._isWithin(x, y, listener)) {
                        listener.onScroll(this._checkWheel(listener.id, evt), evt);
                        break;
                    }
                }
            }
        } else {
            this.listeners
                .get(this.wheelTarget)
                .onScroll(this._checkWheel(this.wheelTarget, evt), evt);
        }
    }

    _notifyZoom(evt: WheelEvent) {
        const order = [...this.parent.getOrder()].reverse();

        for (let i = 0; i < order.length; i++) {
            if (this.listeners.has(order[i])) {
                const listener = this.listeners.get(order[i]);
                listener.onZoom(-evt.deltaY * 0.015,
                    this._computeRelative(evt.pageX, evt.pageY), evt);
            }
        }

    }

    _computeRelative(x:number, y:number) {
        const rect = this.parent.c.getBoundingClientRect();
        const fidel = this.parent.getFidelity();
        return [(x - rect.x) * fidel, (y - rect.y) * fidel];
    }

    _isWithin(x:number, y:number, listener:MouseInputListener): boolean {
        const [ox, oy, w, h] = listener.rect;
        return x >= ox && x <= ox + w && y >= oy && y <= oy + h;
    }
}
