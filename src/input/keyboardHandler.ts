
export interface KeyboardInputListener {
    onKeyPress(evt: KeyboardEvent): void;
    onKeyDown(evt: KeyboardEvent): void;
}


export class KeyboardHandler {
    listeners: KeyboardInputListener[] = [];

    constructor() {
        this.onKeyDown = this.onKeyDown.bind(this);
        this.onKeyPress = this.onKeyPress.bind(this);
    }

    init() {
        window.onkeydown = this.onKeyDown;
        window.onkeypress = this.onKeyPress;
    }

    register(listener: KeyboardInputListener) {
        if (!this.listeners.includes(listener)) this.listeners.push(listener);
    }

    remove(listener: KeyboardInputListener) {
        const i = this.listeners.indexOf(listener);
        if (i >= 0) this.listeners.splice(i, 1);
    }

    onKeyDown(evt: KeyboardEvent) {
        this.listeners.forEach(listener => {
            listener.onKeyDown(evt);
        });
    }

    onKeyPress(evt: KeyboardEvent) {
        this.listeners.forEach(listener => {
            listener.onKeyPress(evt);
        });
    }
}

export const keyboardHandler = new KeyboardHandler();