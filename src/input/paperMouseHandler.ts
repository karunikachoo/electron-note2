import {Paper} from "../paper/paper";

export interface PaperMouseParent {
    getCanvas(): HTMLCanvasElement;
}


export interface PaperMouseListener {
    id: string;

    onClick(mId:number, xy:number[], evt:MouseEvent): void;
    onDoubleClick(mId:number, xy:number[], evt:MouseEvent): void;
    onDrag(mId:number, xy:number[], dxy:number[], evt:MouseEvent): void;
    onHover(xy:number[], evt:MouseEvent): void;
    onMouseEnter(xy:number[], evt:MouseEvent): void;
    onMouseExit(xy:number[], evt:MouseEvent): void;
    onScroll(dxy:number[], evt:WheelEvent): void;
    onZoom(dy:number, xy:number[], evt:WheelEvent): void;
}



export class PaperMouseHandler {
    parent: PaperMouseParent;

    constructor(parent: PaperMouseParent) {
        this.parent = parent;
    }
}