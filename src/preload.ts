// All of the Node.js APIs are available in the preload process.
// It has the same sandbox as a Chrome extension.

import {keyboardHandler} from "./input/keyboardHandler";
import {frameHandler} from "./draw/frameHandler";
import {MainSurface} from "./surface/surfaces/mainSurface";
import {cursor} from "./surface/cursorHandler";

import {PaperObjectHandlerTest} from "./tests/test_paperObjectHandler";
import {TSLog} from "./tests/testSuite";

let main: MainSurface;

function runUnitTests() {
    new PaperObjectHandlerTest().run(null, TSLog.DEBUG);
}

function updateSize() {
    main._updateSize();
}

function init() {
    cursor.init('cursor');
    keyboardHandler.init();

    main = new MainSurface('main');
    main.init();

    frameHandler.start();

    // Run Tests. Comment out in production
    runUnitTests();
}

window.addEventListener("DOMContentLoaded", () => {
    init();

    document.getElementById('body').onresize = updateSize;
});
