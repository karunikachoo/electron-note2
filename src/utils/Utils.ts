
const alphaNum = "abcdefghijklmnopqrstuvwxyz0123456789"
export class Utils {
    static genId(n: number) {
        let ret = "";
        for (let i = 0; i < n; i++) {
            ret += alphaNum[Math.floor(Math.random() * alphaNum.length)];
        }
        return ret;
    }

    static hash(str: string) {
        let hash = 5381;
        let i = str.length;

        while(i) {
            hash = (hash * 33) ^ str.charCodeAt(--i);
        }

        /* JavaScript does bitwise operations (like XOR, above) on 32-bit signed
         * integers. Since we want the results to be always positive, convert the
         * signed int to an unsigned by doing an unsigned bitshift. */
        return hash >>> 0;
    }

    static lerp(fr:number, to:number, dt:number) {
        if (dt > 1) dt = 1;
        if (dt < 0) dt = 0;
        return fr + (to-fr) * dt;
    }

    static forceFontUpdate(ctx: CanvasRenderingContext2D) {
        ctx.fillStyle = '#fff';
        ctx.font = `normal 300 24px Roboto`;
        ctx.fillText('0', -50, -50);

        ctx.font = `normal normal 24px Roboto`;
        ctx.fillText('0', -50, -50);

        ctx.font = `normal bold 24px Roboto`;
        ctx.fillText('0', -50, -50);

        ctx.font = `italic 300 24px Roboto`;
        ctx.fillText('0', -50, -50);

        ctx.font = `italic normal 24px Roboto`;
        ctx.fillText('0', -50, -50);

        ctx.font = `italic bold 24px Roboto`;
        ctx.fillText('0', -50, -50);

        ctx.font = `normal 300 24px RobotoMono`;
        ctx.fillText('0', -50, -50);
    }

    static move(arr: any[], i0: number, i1: number) {
        if (i0 === i1) return arr;

        Utils.clamp(i1, 0, arr.length);

        arr.splice(i1, 0, arr.splice(i0, 1)[0]);
        return arr;
    }

    static clamp(val: number, min: number, max: number) {
        return Math.min(Math.max(val, min), max-1);
    }
}