import {app, BrowserWindow, dialog, ipcMain} from "electron";
import * as path from "path";
import {homedir} from "os";

let mainWindow: BrowserWindow;
const homeFolder = app.getPath("home");

function createWindow() {
    // Create the browser window.
    mainWindow = new BrowserWindow({
        height: 1080,
        webPreferences: {
            preload: path.join(__dirname, "preload.js"),
        },
        width: 1200,
    });
    mainWindow.removeMenu();

    // and load the index.html of the app.
    mainWindow.loadFile(path.join(__dirname, "../static/index.html"));

    // Open the DevTools.
    mainWindow.webContents.openDevTools();
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on("ready", () => {
    createWindow();

    app.on("activate", () => {
        // On macOS it's common to re-create a window in the app when the
        // dock icon is clicked and there are no other windows open.
        if (BrowserWindow.getAllWindows().length === 0) createWindow();
    });
});

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on("window-all-closed", () => {
    if (process.platform !== "darwin") {
        app.quit();
    }
});

// In this file you can include the rest of your app"s specific main process
// code. You can also put them in separate files and require them here.
ipcMain.on('taskbar', (event, args: string) => {
    switch (args) {
        case 'exit':
            mainWindow.close();
            break;
        case 'mini':
            mainWindow.minimize();
            break;
        case 'debug':
            mainWindow.webContents.openDevTools();
            break;
    }

})

ipcMain.on('fs-get-home', (e, arg) => {
    e.reply('fs-get-home', homeFolder);
});

ipcMain.on('fs-dialog-req',(evt, args) =>{
    dialog.showOpenDialog(mainWindow,{
        title: "Open Folder",
        properties: ['openDirectory', 'createDirectory']
    }).then((result) => {
        if (!result.canceled) {
            evt.reply('fs-dialog-ret', result.filePaths);
        }
    })
})