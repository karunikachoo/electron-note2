

class MsgEmitter {

    topics: Map<string, Function[]> = new Map<string, Function[]>();
    onces: Map<string, Function[]> = new Map<string, Function[]>();

    on(topic: string, callback: Function) {
        if (!this.topics.has(topic)) {
            this.topics.set(topic, [callback]);
        } else {
            if (!this.topics.get(topic).includes(callback)) {
                this.topics.get(topic).push(callback);
            }
        }
    }

    once(topic: string, callback: Function) {
        if (!this.onces.has(topic)) {
            this.onces.set(topic, [callback]);
        } else {
            if (!this.onces.get(topic).includes(callback)) {
                this.onces.get(topic).push(callback);
            }
        }
    }

    emit(topic: string, ...data: any) {
        if (this.onces.has(topic)) {
            this.onces.get(topic).forEach(callback => {
                callback(...data);
            })
            this.onces.delete(topic);
        }
        if (this.topics.has(topic)) {
            this.topics.get(topic).forEach(callback => {
                callback(...data);
            })
        }
    }
}

export const msgEmitter = new MsgEmitter();
