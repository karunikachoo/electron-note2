
export interface FrameListener {
    frameInterval: number;
    frameEvent: () => void;
    onFrameUpdate(t: number, dt: number): void;
}

export class FrameHandler {
    listeners: FrameListener[] = [];

    eventCounter: Map<FrameListener, number> = new Map<FrameListener, number>();
    lastUpdate: Map<FrameListener, number> = new Map<FrameListener, number>();

    constructor() {
        this._update = this._update.bind(this);
    }

    start() {
        this._update();
    }

    register(listener: FrameListener) {
        if (!this.listeners.includes(listener)) {
            this.listeners.push(listener);
            this.eventCounter.set(listener, 1);
            this.lastUpdate.set(listener, Date.now());
            listener.frameEvent = () => {
                this.event(listener);
            }
        }
    }

    remove(listener: FrameListener) {
        const i = this.listeners.indexOf(listener);
        if (i >= 0) {
            this.listeners.splice(i, 1);
            this.eventCounter.delete(listener);
            this.lastUpdate.delete(listener);
            listener.frameEvent = null;
        }
    }

    event(listener: FrameListener) {
        this.eventCounter.set(listener, 1);
    }

    _update() {
        const now = Date.now();

        this.listeners.forEach(listener => {
            const dt = now - this.lastUpdate.get(listener);
            const evtCount = this.eventCounter.get(listener);

            if ( evtCount > 0 || dt >= listener.frameInterval ) {
                if (evtCount > 0) this.eventCounter.set(listener, 0);
                listener.onFrameUpdate(now, dt);
                this.lastUpdate.set(listener, now);
            }
        })

        requestAnimationFrame(this._update);
    }

    static intervalFromFPS(fps: number) {
        return 1000 / fps;
    }

    static updateInterval(listener: FrameListener, fps: number) {
        listener.frameInterval = FrameHandler.intervalFromFPS(fps);
    }
}

export const frameHandler = new FrameHandler();