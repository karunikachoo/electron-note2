import {Paper} from "../paper/paper";
import {PaperBaseObject} from "../paper/paperBaseObject";
import {TestState, TestSuite} from "./testSuite";
import {POLayer} from "../paper/paperObjectHandler";
import {Utils} from "../utils/Utils";


export class PaperObjectHandlerTest extends TestSuite{

    /** testObjectAdd Tests basic object adding
     *
     * Expected outcome:
     * - Objects are added successfully
     * - PaperObjectHandler.objects contain all ids
     */
    testObjectAdd(suite: PaperObjectHandlerTest) {
        const asserts = Number(suite.assertFailed);

        const paper = new Paper('main');
        const handler = paper.objectHandler;

        const ids = ["0x1", "0x2", "0x3", "0x4", "0x5", "0x6"]
        for (let i = 0; i < ids.length; i++) {
            const o = new PaperBaseObject(paper, ids[i]);
            handler.add(o);
        }

        for (let i = 0; i < ids.length; i++) {
            if (suite.assert(handler.objects.has(ids[i]), "Handler does not have ID\n")) return;
            const o = handler.get(ids[i]);

            suite.logdbg(`${o.id} === ${ids[i]}\n`)
            if (suite.assert(o.id === ids[i], "Diff id\n")) return;
        }

        return asserts !== suite.assertFailed  ? TestState.FAILED : TestState.SUCCESS;
    }

    /** testObjectRemove Tests basic object removal
     *
     * Expected outcome:
     * - Objects are added successfully
     * - Objects are removed successfully
     * - PaperObjectHandler.objects no longer contain ids
     * - PaperObjectHandler.layers no longer contain ids
     */
    testObjectRemove(suite: PaperObjectHandlerTest) {
        const asserts = Number(suite.assertFailed);

        const paper = new Paper('main');
        const handler = paper.objectHandler;

        const ids = ["0x1", "0x2", "0x3", "0x4", "0x5", "0x6"]

        for (let i = 0; i < ids.length; i++) {
            const o = new PaperBaseObject(paper, ids[i]);
            handler.add(o);
        }

        for (let i = 0; i < ids.length; i++) {
            if (suite.assert(handler.objects.has(ids[i]), `Handler does not have ${ids[i]}\n`)) return;
            if (suite.assert(handler.layers.get(POLayer.BASE).includes(ids[i]), `Handler Layer does not have ${ids[i]}\n`)) return;

            handler.remove(ids[i]);

            if (suite.assert(!handler.objects.has(ids[i]), `Handler did not remove ${ids[i]}\n`)) return;
            if (suite.assert(!handler.layers.get(POLayer.BASE).includes(ids[i]), `Handler Layer did not remove ${ids[i]}\n`)) return;
        }

        return suite.assertFailed !== asserts ? TestState.FAILED : TestState.SUCCESS;
    }

    /** differentLayerNumbers Tests adding objects of different layer types
     *
     * Expected outcome:
     * - Objects are added successfully
     * - Only objects with layers > than POLayer.CHILD are added
     * - The resultant order ids matches expected ids
     */
    differentLayerNumbers(suite: PaperObjectHandlerTest) {
        const asserts = Number(suite.assertFailed);

        const paper = new Paper('main');
        const handler = paper.objectHandler;

        const layers = [POLayer.NO_LAYER, 0, 1, 2, 3, POLayer.OVERLAY];
        const expectedIds:string[] = [];

        layers.forEach((layer, i) => {
            const id = `0x${i}`;
            if(layer > POLayer.NO_LAYER) expectedIds.push(id);

            const o = new PaperBaseObject(paper, id);
            o.setLayer(layer);
            suite.logdbg(`${o.id}.layerNumber = ${layer}\n`);

            handler.add(o);
        })

        const order = handler.getOrder();

        suite.logdbg('order: [' + order.toString() + ']\n');

        for (let i = 0; i < order.length; i++) {
            const id = order[i];
            const ind = expectedIds.indexOf(id);
            if (ind >= 0) {
                expectedIds.splice(ind, 1);
            } else {
                suite.assert(false, `Unexpected id: ${id}\n`);
                return;
            }
        }

        expectedIds.forEach(id => {
            suite.assert(false, `Missing expected id: ${id}\n`);
        })

        return suite.assertFailed !== asserts ? TestState.FAILED : TestState.SUCCESS;
    }

    /** multipleSameLayer Test adding multiple objects with same layer number
     * Tests new layer and existing layer handling
     * tests non-visible layers as well
     *
     * Expects:
     * - non-visible layers are not added
     * - order ids are ordered by layers
     */
    multipleSameLayer(suite: PaperObjectHandlerTest) {
        const asserts = Number(suite.assertFailed);

        const paper = new Paper('main');
        const handler = paper.objectHandler;

        const layers = [POLayer.NO_LAYER, 11, 3, POLayer.OVERLAY];
        let count = 0;

        layers.forEach((layer, i) => {
            for (let j = 0; j < 2; j++) {
                const id = `0x${count}`;

                const o = new PaperBaseObject(paper, id);
                o.setLayer(layer);
                suite.logdbg(`${o.id}.layerNumber = ${layer}\n`);

                handler.add(o);
                count++;
            }
        })

        suite.logdbg(`layers: [ ${ handler.layerKeys } ]\n\n`);
        const order = handler.getOrder();
        suite.logdbg(`order: [ ${ order } ]\n`);
        let lastLayer = 0;

        for (let i = 0; i < order.length; i++) {
            const id = order[i];
            const o = handler.get(id);
            suite.logdbg(`${id} : ${lastLayer} <= ${o.layerNumber}\n`);
            if (suite.assert(lastLayer <= o.layerNumber,
                `Layer out of order ${lastLayer} !<= ${o.layerNumber}\n`)) return;
            lastLayer = o.layerNumber;
        }
        return suite.assertFailed !== asserts ? TestState.FAILED : TestState.SUCCESS;
    }

    /** orderShifting Test for shifting paper object ordering
     *
     * for LAYER UNDERLAY, BASE, OVERLAY add 4 PaperBaseObject
     * for each PaperBaseObject test shiftUp, shiftDown, shiftTop, shiftBottom
     *
     * Expects:
     * - non-visible layers are not added
     * - order ids are ordered by layers
     */
    orderShifting(suite: PaperObjectHandlerTest) {
        const asserts = Number(suite.assertFailed);

        const paper = new Paper('main');
        const handler = paper.objectHandler;

        const layers = [1, 2, 99];
        const ids: string[] = [];


        let idCount = 0;
        layers.forEach(layer => {
            for (let j = 0; j < 4; j++) {
                const id = `0x${idCount}`;
                ids.push(id);

                const o = new PaperBaseObject(paper, id);
                o.setLayer(layer);
                suite.logdbg(`${o.id}.layerNumber = ${layer}\n`);

                handler.add(o);
                idCount++;
            }
        })

        for (let n = 0; n < ids.length; n++) {
            const id = ids[n];

            let i: number;
            let ei: number;
            let ri: number;

            suite.logdbg('\n');

            i = handler.getObjectOrder(id)
            ei = Utils.clamp(i + 1, 0, 4);
            handler.shiftUp(id);
            ri = handler.getObjectOrder(id);
            suite.logdbg(`${id} : ${i} -> ${ri} == ${ei}\n`);
            if(suite.assert(ri === ei, `${id} : ${i} -> ${ri} == ${ei}\n`)) return;

            i = handler.getObjectOrder(id);
            ei = Utils.clamp(i - 1, 0, 4);
            handler.shiftDown(id);
            ri = handler.getObjectOrder(id);
            suite.logdbg(`${id} : ${i} -> ${ri} == ${ei}\n`);
            if(suite.assert(ri === ei, `${id} : ${i} -> ${ri} == ${ei}\n`)) return;

            i = handler.getObjectOrder(id);
            ei = 3;
            handler.shiftTop(id);
            ri = handler.getObjectOrder(id);
            suite.logdbg(`${id} : ${i} -> ${ri} == ${ei}\n`);
            if(suite.assert(ri === ei, `${id} : ${ri} == ${ei}\n`)) return;

            i = handler.getObjectOrder(id);
            ei = 0;
            handler.shiftBottom(id);
            ri = handler.getObjectOrder(id);
            suite.logdbg(`${id} : ${i} -> ${ri} == ${ei}\n`);
            if(suite.assert(ri === ei, `${id} : ${ri} == ${ei}\n`)) return;
        }

        return suite.assertFailed !== asserts ? TestState.FAILED : TestState.SUCCESS;
    }

    addRemoveChild(suite: PaperObjectHandlerTest) {

    }


    constructor() {
        super();
        this.registerTest(0x01, "Test Object Add", this.testObjectAdd);
        this.registerTest(0x02, "Test Object Remove", this.testObjectRemove);
        this.registerTest(0x03, "Different Layer numbers", this.differentLayerNumbers);
        this.registerTest(0x04, "Multiple objects with same layer numbers", this.multipleSameLayer);
        this.registerTest(0x05, "Order Shifting", this.orderShifting);
    }

}