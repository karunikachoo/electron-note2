export enum TestState {
    ERROR = -1,
    FAILED,
    SUCCESS
}

export enum TSLog {
    FATAL,
    ALERT,
    FAIL,
    WARN,
    INFO,
    DEBUG
}

export type TestFunction = (suite: TestSuite) => TestState;

const BAR = "=====================================\n";

export class TestSuite {
    stdout: string = "";
    tests: Map<number, TestFunction> = new Map<number, TestFunction>();
    names: Map<number, string> = new Map();

    logLevel = TSLog.INFO;

    assertFailed = 0;
    testsFailed = 0;

    registerTest(id:number, name:string, test:TestFunction) {
        this.tests.set(id, test);
        this.names.set(id, name);
    }

    log(level: TSLog, message: string) {
        if (level <= this.logLevel) this.stdout += "  " + message;
    }

    logdbg(message: string) {
        this.log(TSLog.DEBUG, message);
    }

    assert(e: boolean, message: string) {
        if (!e) {
            this.assertFailed++;
            this.log(TSLog.FAIL, "ASSERT_FAILED: " + message);
        }
        return !e;
    }

    run(ids?: number[], logLevel?: TSLog): void {
        this.logLevel = logLevel | TSLog.INFO;

        this.stdout = "";
        this.testsFailed = 0;
        this.assertFailed = 0;

        this.stdout += "\n";
        this.stdout += BAR;
        this.stdout += `  Suite: ${this.constructor.name}\n`;
        this.stdout += BAR

        ids = ids ? ids : Array.from(this.tests.keys());

        ids.forEach(id => {
            if (this.tests.has(id)) {
                const test = this.tests.get(id);
                this.stdout += BAR;
                this.stdout += `  Test ${id}: ${this.names.get(id)}\n`;
                this.stdout += BAR;

                const res = test(this);
                if (res !== TestState.SUCCESS) {
                    this.testsFailed++;
                }
            }
        })

        this.stdout += BAR;
        this.stdout += "  RESULTS\n"
        this.stdout += BAR;
        this.stdout += `  asserts failed: ${this.assertFailed}\n`;
        this.stdout += `    tests failed: ${this.testsFailed}\n`;
        this.stdout += `    tests passed: ${ids.length - this.testsFailed}\n`;
        this.stdout += BAR;
        console.log(this.stdout);
    }
}