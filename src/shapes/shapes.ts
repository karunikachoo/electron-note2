

export class Shapes {
    static roundedRect(ctx: CanvasRenderingContext2D,
                       x:number, y:number, w:number, h:number, r:number) {
        ctx.moveTo(x, y + h - r);

        ctx.arcTo(x, y, x + r, y, r);
        ctx.arcTo(x + w, y, x + w, y + r, r);
        ctx.arcTo(x + w, y + h, x + w - r, y + h, r);
        ctx.arcTo(x, y + h, x, y + h - r, r);
    }
}