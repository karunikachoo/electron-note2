import {SurfaceObject} from "../../surface/surfaceObject";
import {MouseInputListener} from "../../input/mouseHandler";
import {Surface} from "../../surface/surface";
import {PatternPath2D, TilerPatterns} from "./tilerPatterns";
import {SurfaceConstants} from "../../surface/surfaceConstants";
import {cursor} from "../../surface/cursorHandler";

export type OnZoomCallback = (scale: number) => void;
export type OnPanCallback = (xy: number[]) => void;

export class SurfacePatternTilerObject extends SurfaceObject implements MouseInputListener {

    /** flags */
    fixed = true;

    /** pattern tiler */
    pattern: PatternPath2D = TilerPatterns.loadPattern(TilerPatterns.TRIANGLES);
    rasterPath: Path2D = new Path2D();
    rasterTile: HTMLCanvasElement = document.createElement('canvas');
    rasterCanvas: HTMLCanvasElement = document.createElement('canvas');

    rasterCanvases: HTMLCanvasElement[] = [];
    rasterBrackets: number[] = [];

    transformMatrix = document.createElementNS("http://www.w3.org/2000/svg", "svg").createSVGMatrix();

    /** Input Handling */
    onZoomCallback: OnZoomCallback;
    onPanCallback: OnPanCallback;

    // Relinquish zoom and pan control to this object via onZoom and onPan callbacks
    constructor(parent: Surface, onZoom?: OnZoomCallback, onPan?: OnPanCallback) {
        super(parent)
        this.parent.mouseHandler.register(this);
        this.rect[2] = this.parent.c.width;
        this.rect[3] = this.parent.c.height;
        this.onZoomCallback = onZoom;
        this.onPanCallback = onPan;

        this.refresh();
    }

    setBackgroundColor(color: string) {
        // TODO: this.parent.c.style.setProperty()
    }

    setForegroundColor(color: string) {
        // TODO: foreground color
        //       also a settings panel for patterns + color
    }

    refresh() {
        this._initRasterCanvases();
        this._rasterTile(this.parent.zoomMax);
        this._rasterSections();
    }

    _initRasterCanvases() {
        const [zmin, zmax] = [Math.ceil(this.parent.zoomMin), Math.ceil(this.parent.zoomMax)];

        if (this.parent.zoomMin < 0.25) {
            this.parent.zoomMin = 0.25;
        }

        this.rasterBrackets.push(0.25);
        this.rasterCanvases.push(document.createElement('canvas'));

        for (let i = zmin; i < zmax; i++) {
            this.rasterBrackets.push(i);
            this.rasterCanvases.push(document.createElement('canvas'));
        }
    }

    _rasterTile(s:number) {
        this.rasterTile.width = this.pattern.width * s;
        this.rasterTile.height = this.pattern.height * s;

        const m = this.transformMatrix.scale(s, s);
        this.rasterPath = new Path2D();
        this.rasterPath.addPath(this.pattern, m);

        const rctx = this.rasterTile.getContext('2d');
        rctx.fillStyle = "#ffffff0a";
        rctx.fill(this.rasterPath);
    }

    _rasterSections() {
        this.rasterBrackets.forEach((bracket, i) => {
            const s = bracket;

            const [pw, ph] = [
                this.pattern.width * this.pattern.baseScale * s,
                this.pattern.height * this.pattern.baseScale * s
            ]

            const [nx, ny] = [
                Math.ceil( this.parent.getWidth() / pw) + 2,
                Math.ceil( this.parent.getHeight() / ph) + 2,
            ];

            this.rasterCanvases[i].width = nx * pw;
            this.rasterCanvases[i].height = ny * ph;

            const rtx = this.rasterCanvases[i].getContext('2d');

            for (let row = 0; row < ny; row++) {
                for (let col = 0; col < nx; col++) {
                    rtx.drawImage(this.rasterTile, pw * col, ph * row, pw, ph);
                }
            }
        })
    }

    draw(ctx: CanvasRenderingContext2D, ox: number, oy: number, s: number, f: number) {
        const [dx, dy] = [
            ox % (this.pattern.width * this.pattern.baseScale),
            oy % (this.pattern.height * this.pattern.baseScale)
        ];

        let i;
        for (i = 0; i < this.rasterBrackets.length; i++) {
            if (this.rasterBrackets[i] > s) break;
        }
        i--;
        const bracketTop = this.rasterBrackets[i];

        const [ddx, ddy, ddw, ddh] = [
            (-2 * this.pattern.width + dx) * s,
            (-2 * this.pattern.height + dy) * s,
            this.rasterCanvases[i].width / bracketTop * s ,
            this.rasterCanvases[i].height / bracketTop * s
        ];

        ctx.drawImage(this.rasterCanvases[i], ddx, ddy, ddw, ddh);
    }

    onScroll(dxy: number[], evt: WheelEvent): void {
        this._pan(dxy);
    }

    _pan(dxy:number[]) {
        const s = this.parent.getScale();
        const [ox, oy] = this.parent.getOffset();
        if (this.onPanCallback) {
            this.onPanCallback(
                [
                    ox + dxy[0] * this.parent.fidel / s,
                    oy + dxy[1] * this.parent.fidel / s
                ]
            )
        }
    }

    onZoom(dy: number, xy: number[], evt: WheelEvent): void {
        this.parent.setZoom(this.parent.getScale() + dy, xy[0], xy[1]);
    }

    onClick(mId: number, xy: number[], evt: MouseEvent): void {}
    onDoubleClick(mId: number, xy: number[], evt: MouseEvent): void {}
    onDrag(mId: number, xy: number[], dxy: number[], evt: MouseEvent): void {
        cursor.reset();
        this._pan(dxy);
    }
    onHover(xy: number[], evt: MouseEvent): void {
        cursor.reset();
    }

    onMouseEnter(xy: number[], evt: MouseEvent): void {
    }

    onMouseExit(xy: number[], evt: MouseEvent): void {
    }

}