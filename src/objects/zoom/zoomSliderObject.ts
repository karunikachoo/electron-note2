
import {Surface} from "../../surface/surface";
import {SurfaceConstants} from "../../surface/surfaceConstants";
import {msgEmitter} from "../../comm/msgEmitter";
import {Shapes} from "../../shapes/shapes";
import {SurfaceAnchorObject} from "../../surface/objects/surfaceAnchorObject";


export class ZoomSliderObject extends SurfaceAnchorObject {

    fixed = true;
    overlay = true;

    anchorFollowOffsets = true;

    scale: number = 1;
    zoomSetTopic:string = "main-zoom-set";
    zoomUpdateTopic:string = "main-zoom-update";

    /** Slider value */
    constructor(parent: Surface) {
        super(parent);

        msgEmitter.on(this.zoomUpdateTopic, (scale: number) => {
            this.scale = scale;
        })
    }

    draw(ctx: CanvasRenderingContext2D, ox: number, oy: number, s: number, f: number) {
        const [rx, ry, w, h] = this.getAnchorOffsetRect();
        const percent = (this.scale - SurfaceConstants.ZOOM_MIN) / SurfaceConstants.ZOOM_RANGE;
        const y = (1 - percent) * h;

        ctx.save();
        ctx.beginPath();

        Shapes.roundedRect(ctx, rx, ry, w, h, this.radius);
        ctx.clip();

        ctx.fillStyle = SurfaceConstants.OVERLAY_BG_COL;
        ctx.fill();

        ctx.fillStyle = SurfaceConstants.OVERLAY_PROGRESS_COL;
        ctx.fillRect(rx, ry + y, w, h - y);

        ctx.beginPath();
        ctx.strokeStyle = SurfaceConstants.OVERLAY_ICON_COL;
        ctx.lineWidth = 3;
        ctx.arc(rx + w * 2.25/4, ry + h - w * 2.25/4, 15, 0, 2 * Math.PI);
        ctx.moveTo(rx + w * 1.6/4, ry + h - w * 2.25/4 + 12);
        ctx.lineTo(rx + w/4, ry + h - w/4);
        ctx.stroke();

        // const perc = (this.scale * 100);
        //
        // ctx.fillStyle = y < this.rect[3]/2 - 24/2 ? "#333" : "#fff";
        // ctx.font = "300 24px Roboto";
        // ctx.textAlign = "center"
        // ctx.textBaseline = "middle"
        // ctx.fillText(`${perc.toFixed(0)}%`,
        //     this.rect[0] + this.rect[2]/2,
        //     this.rect[1] + this.rect[3]/2)
        ctx.restore();
    }

    onScroll(dxy: number[], evt: WheelEvent): void {
        msgEmitter.emit(this.zoomSetTopic, this.scale - dxy[1] * 0.0005);
    }

    onDrag(mId: number, xy: number[], dxy: number[], evt: MouseEvent) {
        super.onDrag(mId, xy, dxy, evt);
        msgEmitter.emit(this.zoomSetTopic, this.scale - dxy[1] * 0.020);
    }

    onClick(mId: number, xy: number[], evt: MouseEvent) {
        super.onClick(mId, xy, evt);
    }

    onDoubleClick(mId: number, xy: number[], evt: MouseEvent): void {
        msgEmitter.emit(this.zoomSetTopic, 1);
    }

}