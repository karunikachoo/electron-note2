import {Surface} from "../../surface/surface";
import {Tool} from "./tool";
import {AddNote} from "./addNote";


export class Toolbar {
    parent: Surface;
    tools: Tool[] = [];

    padding = 20;
    width = 75;
    height = 75;

    rect: number[] = [0, 0, 0, 0];

    constructor(parent: Surface) {
        this.parent = parent;
    }

    /** position is
     * x = w - 95
     * y = h/2 - this.rect[3]/2
     * w = 75
     * h = 75
     * padd = 10
     */

    init() {
        const addNote = new AddNote(this.parent);
        this.parent.add(addNote);
        this.tools.push(addNote);

        for(let i = 0; i < 5; i++) {
            const n = new Tool(this.parent);
            this.parent.add(n);
            this.tools.push(n);
        }
    }

    updateDimensions() {
        const calcHeight = this.tools.length * this.height + (this.tools.length - 1) * this.padding;

        const [x, y] = [
            this.parent.getWidth() - 95,
            (this.parent.getHeight() - calcHeight) / 2
        ]

        this.tools.forEach((tool, i) => {
            tool.setRect(x, y + i * (this.height + this.padding), this.width, this.height);
        })

        this.rect = [x, y, this.width, calcHeight];
    }

}