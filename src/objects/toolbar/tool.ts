import {SurfaceAnchorObject} from "../../surface/objects/surfaceAnchorObject";
import {Shapes} from "../../shapes/shapes";
import {SurfaceConstants} from "../../surface/surfaceConstants";

export class Tool extends SurfaceAnchorObject {
    /** flags */
    fixed = true;
    overlay = true;

    anchorFollowOffsets = true;

    toolActive = false;

    draw(ctx: CanvasRenderingContext2D, ox: number, oy: number, s: number, f: number) {
        const [rx, ry, w, h] = this.getAnchorOffsetRect();

        ctx.save();
        ctx.beginPath();

        Shapes.roundedRect(ctx, rx, ry, w, h, this.radius);
        ctx.fillStyle = this.toolActive ? SurfaceConstants.OVERLAY_PROGRESS_COL : SurfaceConstants.OVERLAY_BG_COL;
        ctx.fill()

        ctx.restore();
    }
}