import {Tool} from "./tool";
import {SurfaceConstants} from "../../surface/surfaceConstants";


export class AddNote extends Tool {
    draw(ctx: CanvasRenderingContext2D, ox: number, oy: number, s: number, f: number) {
        super.draw(ctx, ox, oy, s, f);

        const [rx, ry, w, h] = this.getAnchorOffsetRect();
        const v = w/8;
        const [cx, cy] = [rx + w/2, ry + h/2];

        ctx.save();
        ctx.beginPath();

        ctx.strokeStyle = SurfaceConstants.OVERLAY_ICON_COL;

        ctx.moveTo(cx - 3 * v, cy - 2 * v);
        ctx.lineTo(cx - v, cy - 2 * v);
        ctx.moveTo(cx - 2 * v, cy - 3 * v);
        ctx.lineTo(cx - 2 * v, cy - v);

        ctx.stroke();

        ctx.restore();
    }

    onClick(mId: number, xy: number[], evt: MouseEvent) {
        super.onClick(mId, xy, evt);

        console.log("addNote");
    }
}