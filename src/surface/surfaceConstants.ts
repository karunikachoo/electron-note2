

export class SurfaceConstants {
    static readonly FIDEL_MULT = 2;

    static readonly ZOOM_MIN = 0.5;
    static readonly ZOOM_MAX = 4;
    static readonly ZOOM_RANGE = SurfaceConstants.ZOOM_MAX - SurfaceConstants.ZOOM_MIN;

    static readonly OVERLAY_BG_COL = "#00000033";
    static readonly OVERLAY_PROGRESS_COL = "#FFFFFF88";
    static readonly OVERLAY_ICON_COL = "#dddddda8";
}