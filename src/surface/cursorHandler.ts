import {msgEmitter} from "../comm/msgEmitter";


/**
 * iPadOS style cursor https://codepen.io/cezzre/pen/poJxLEM
 */

class CursorHandler {
    e: HTMLDivElement;

    isReset: boolean = false;
    locked: boolean = false;
    x:number = 0;
    y:number = 0;

    init(id: string) {
        this.e = document.getElementById(id) as HTMLDivElement;

        window.addEventListener('mousemove', ({x, y}) => {
            this.updatePosition(x, y);
            return true;
        })

        this.e.addEventListener('transitionend', () => {
            if (!this.locked && this.isReset) {
                this.e.style.setProperty('--transform', 'translate(-50%, -50%)');
                this.isReset = false;
            }
        })

        this.updatePosition = this.updatePosition.bind(this);

        // msgEmitter.on('mouse-move', this.updatePosition)
    }

    reset() {
        this.locked = false;
        this.isReset = true;
        this.e.style.setProperty('--width', 1.25 + 'rem');
        this.e.style.setProperty('--height', 1.25 + 'rem');
        this.e.style.setProperty('--radius', 1.25 + 'rem');
        this.e.style.setProperty('--opacity', '0.2');
    }

    setShape(w:number, h:number, r:number) {
        this.locked = true;

        this.e.style.setProperty('--width', w + 'px');
        this.e.style.setProperty('--height', h + 'px');
        this.e.style.setProperty('--radius', r + 'px');

        this.e.style.setProperty('--opacity', '0.08');
    }

    setTransform(px:number, py:number) {
        this.e.style.setProperty('--transform', `translate(${px}%, ${py}%)`);
    }

    updatePosition(x:number, y:number) {
        this.e.style.setProperty('--left', x + 'px');
        this.e.style.setProperty('--top', y + 'px');
    }
}

export const cursor = new CursorHandler();