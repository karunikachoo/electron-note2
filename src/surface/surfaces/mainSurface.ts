import {Surface} from "../surface";
import {SurfacePatternTilerObject} from "../../objects/tiler/surfacePatternTilerObject";
import {msgEmitter} from "../../comm/msgEmitter";
import {frameHandler} from "../../draw/frameHandler";
import {Utils} from "../../utils/Utils";
import {ZoomSliderObject} from "../../objects/zoom/zoomSliderObject";
import {Toolbar} from "../../objects/toolbar/toolbar";


export class MainSurface extends Surface {
    tiler: SurfacePatternTilerObject;
    zoomSlider: ZoomSliderObject;
    toolbar: Toolbar = new Toolbar(this);

    constructor(id:string) {
        super(id);
    }

    init() {
        super.init();

        this.tiler = new SurfacePatternTilerObject(this, this.setScale, this.setOffset);
        this.add(this.tiler);

        this.zoomSlider = new ZoomSliderObject(this);
        this.zoomSlider.setRect(this.getWidth() - 200, 200, 100, 500);
        this.add(this.zoomSlider);

        this.toolbar.init();

        msgEmitter.on('main-zoom-set', (scale:number) => {
            this.setZoom(scale, this.getWidth()/2, this.getHeight()/2);
        })

        this._updateSize();
    }

    setScale(scale: number) {
        super.setScale(scale);
        msgEmitter.emit('main-zoom-update', scale);
    }

    _updateSize() {
        super._updateSize();
        if (this.tiler) {
            this.tiler.setWidth(this.c.width);
            this.tiler.setHeight(this.c.height);
            this.tiler.refresh();
        }

        this.toolbar.updateDimensions();

        if (this.zoomSlider) {
            this.zoomSlider.setRect(this.c.width-95, 95, 75, this.toolbar.rect[1] - 95 - this.toolbar.padding * 2);
        }
    }

}