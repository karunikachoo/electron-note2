import {MouseHandler} from "../input/mouseHandler";
import {KeyboardHandler} from "../input/keyboardHandler";
import {SurfaceConstants} from "./surfaceConstants";
import {SurfaceObject} from "./surfaceObject";
import {frameHandler, FrameHandler, FrameListener} from "../draw/frameHandler";
import {Utils} from "../utils/Utils";

// SURFACES DO NOT DRAW ANYTHING. LEAVE THAT TO SURFACE OBJECTS

export class Surface implements FrameListener {
    /** Internal params */
    id: string = Utils.genId(8);
    c: HTMLCanvasElement;

    /** Frame Listener */
    frameInterval: number = FrameHandler.intervalFromFPS(2);
    frameEvent: () => void;

    /** InputHandlers */
    mouseHandler: MouseHandler = new MouseHandler(this);

    /** Objects */
    objects: Map<string, SurfaceObject> = new Map<string, SurfaceObject>();
    order: string[] = [];
    overlay: string[] = [];

    /** Offset & Scale & fidelity */
    offset = [0, 0];
    scale = 1;
    zoomMin = SurfaceConstants.ZOOM_MIN;
    zoomMax = SurfaceConstants.ZOOM_MAX;
    fidel = SurfaceConstants.FIDEL_MULT;

    constructor(id: string) {
        this.id = id;
        this.setScale = this.setScale.bind(this);
        this.setOffset = this.setOffset.bind(this);
    }

    event() {frameHandler.event(this)};

    init() {
        this.c = document.getElementById(this.id) as HTMLCanvasElement;

        this.mouseHandler.init();

        this._updateSize();

        frameHandler.register(this);

        Utils.forceFontUpdate( this.c.getContext('2d') );
    }

    setOffset(offset:number[]) {
        this.offset = offset;
        frameHandler.event(this);
    }

    setZoom(scale:number, x:number, y:number) {
        scale = Utils.clamp(scale, this.zoomMin, this.zoomMax);
        const oldScale = this.scale;

        this.setScale(scale);

        this.offset[0] += (x / scale) - (x / oldScale);
        this.offset[1] += (y / scale) - (y / oldScale);
        frameHandler.event(this);
    }

    setScale(scale: number) {
        this.scale = Utils.clamp(scale, this.zoomMin, this.zoomMax);
    }
    setFidelity(fidel:number) { this.fidel = fidel; }

    getObject(id: string) {
        if (this.objects.has(id)) return this.objects.get(id);
        return null;
    }
    getOffset() { return this.offset; }
    getScale() { return this.scale; }
    getFidelity() { return this.fidel; }
    getWidth() { return this.c.width;}
    getHeight() { return this.c.height;}

    getOrder() { return [...this.order, ...this.overlay]; }

    add(object: SurfaceObject) {
        this.objects.set(object.id, object);
        if (object.overlay) {
            if (!this.overlay.includes(object.id)) this.overlay.push(object.id);
        } else {
            if (!this.order.includes(object.id)) this.order.push(object.id)
        }
    }

    remove(val: string | SurfaceObject) {
        let id = "";
        if (val instanceof SurfaceObject) id = val.id;
        else id = val;

        if (this.objects.has(id)) {
            this.objects.delete(id);
            const i = this.order.indexOf(id);
            if (i >= 0) this.order.splice(i, 1);
            const j = this.overlay.indexOf(id);
            if (j >= 0) this.overlay.splice(i, 1);
        }
    }

    /** shifting order */
    shiftForward(id: string) {
        // go towards back of order
        const i = this.order.indexOf(id);
        let n = i + 1;
        if (i >= 0) {
            if (n >= this.order.length - 1) n = this.order.length - 1;
            this.order = Utils.move(this.order, i, n);
        }
    }

    shiftBackward(id: string) {
        const i = this.order.indexOf(id);
        let n = i - 1;
        if (i >= 0) {
            if (n <= 0) n = 0;
            this.order = Utils.move(this.order, i, n);
        }
    }

    shiftToFront(id: string) {
        const i = this.order.indexOf(id);
        const n = this.order.length - 1;
        if (i >= 0) {
            this.order = Utils.move(this.order, i, n);
        }
    }

    shiftToBack(id: string) {
        const i = this.order.indexOf(id);
        const n = 0
        if (i >= 0) {
            this.order = Utils.move(this.order, i, n);
        }
    }

    /** draw */
    _updateSize() {
        const rect = this.c.getBoundingClientRect();
        this.c.width = rect.width * this.fidel;
        this.c.height = rect.height * this.fidel;
        frameHandler.event(this);
    }

    _limitX(x: number) { return x < 0 ? 0 : (x > this.c.width ? this.c.width : x); }
    _limitY(y: number) { return y < 0 ? 0 : (y > this.c.height ? this.c.height : y); }
    _isVisible(x0: number, y0: number, x1: number, y1: number) {
        x0 = this._limitX(x0);
        x1 = this._limitX(x1);
        y0 = this._limitY(y0);
        y1 = this._limitY(y1);

        return (x1 - x0) * (y1 - y0) > 0;
    }

    _checkDraw(so: SurfaceObject) {
        if (so.fixed) {
            const [x0, y0] = [so.rect[0], so.rect[1]];
            const [x1, y1] = [x0 + so.rect[2], y0 + so.rect[3]];
            return this._isVisible(x0, y0, x1, y1);
        } else {
            const [sx0, sy0, sx1, sy1] = so.getScaledOffsetPoints();
            return this._isVisible(sx0, sy0, sx1, sy1);
        }
    }

    draw(ctx: CanvasRenderingContext2D, ox: number, oy: number, s: number, f: number) {
        this.getOrder().forEach(id => {
            const so = this.objects.get(id);
            if (so) {
                if (this._checkDraw(so)) {
                    so.draw(ctx, ox, oy, s, f);
                }
            }
        })
    }

    render() {
        if (!this.c) return;

        const ctx = this.c.getContext('2d');
        ctx.clearRect(0, 0, this.c.width, this.c.height);

        this.draw(ctx, this.offset[0], this.offset[1], this.scale, this.fidel);
    }

    onFrameUpdate(t: number, dt: number): void {
        this.render();
    }
}