import {SurfaceObject} from "../surfaceObject";
import {Surface} from "../surface";
import {MouseInputListener} from "../../input/mouseHandler";
import {KeyboardInputListener} from "../../input/keyboardHandler";


class SurfaceTransformObject extends SurfaceObject implements MouseInputListener, KeyboardInputListener {
    // Add Hover Handler & or click handler

    constructor(parent: Surface) {
        super(parent);
        this.parent.mouseHandler.register(this);
    }

    activate() {

    }

    deactivate() {

    }

    onClick(mId: number, xy: number[], evt: MouseEvent): void {
    }

    onDoubleClick(mId: number, xy: number[], evt: MouseEvent): void {
    }

    onDrag(mId: number, xy: number[], dxy: number[], evt: MouseEvent): void {
    }

    onHover(xy: number[], evt: MouseEvent): void {
    }

    onScroll(dxy: number[], evt: WheelEvent): void {
    }


    onZoom(dy: number, xy: number[], evt: WheelEvent): void {

    }

    /** Add support for arrow key nudging */

    onKeyDown(evt: KeyboardEvent): void {
    }

    onKeyPress(evt: KeyboardEvent): void {
    }

    onMouseEnter(xy: number[], evt: MouseEvent): void {
    }

    onMouseExit(xy: number[], evt: MouseEvent): void {
    }
}