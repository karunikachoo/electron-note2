import {SurfaceObject} from "../surfaceObject";
import {MouseInputListener} from "../../input/mouseHandler";
import {Surface} from "../surface";
import {cursor} from "../cursorHandler";
import {Utils} from "../../utils/Utils";


export class SurfaceAnchorObject extends SurfaceObject implements MouseInputListener {

    /** flags */
    anchorActive = true;
    anchorFollowOffsets = false;

    /** Props */
    radius:number = 10;
    anchorPadding:number = 6;

    anchorOffsets = [0, 0];
    anchorJiggleMult = 1.0;

    constructor(parent: Surface) {
        super(parent);

        this.parent.mouseHandler.register(this);
    }


    onClick(mId: number, xy: number[], evt: MouseEvent): void {
    }

    onDoubleClick(mId: number, xy: number[], evt: MouseEvent): void {
    }

    onDrag(mId: number, xy: number[], dxy: number[], evt: MouseEvent): void {
        this._updateCursor(xy);
    }

    onHover(xy: number[], evt: MouseEvent): void {
        this._updateCursor(xy);
    }

    _linearOffsets(xy:number[]) {
        const half = [this.rect[2] / 2, this.rect[3] / 2]
        this.anchorOffsets = [
            (xy[0] - this.rect[0] - half[0]) / half[0] * this.anchorPadding,
            (xy[1] - this.rect[1] - half[1]) / half[1] * this.anchorPadding
        ]
        this.anchorOffsets = [
            Utils.clamp(this.anchorOffsets[0], -this.anchorPadding, this.anchorPadding),
            Utils.clamp(this.anchorOffsets[1], -this.anchorPadding, this.anchorPadding)
        ]
    }

    _exponentialOffset(xy:number[]) {
        const half = [this.rect[2] / 2, this.rect[3] / 2]
        const d = [
            (xy[0] - this.rect[0] - half[0]) / half[0],
            (xy[1] - this.rect[1] - half[1]) / half[1]
        ]

        const perc = [
            -1 / Math.exp(Math.abs(d[0]) * 5) + 1,
            -1 / Math.exp(Math.abs(d[1]) * 5) + 1
        ]

        this.anchorOffsets = [
            this.anchorPadding * perc[0] * (d[0] < 0 ? -1 : 1),
            this.anchorPadding * perc[1] * (d[1] < 0 ? -1 : 1)
        ]
    }

    _updateCursor(xy:number[], offset:boolean=true) {
        if (this.anchorActive) {
            if (this.anchorFollowOffsets && offset) {
                this._linearOffsets(xy);

                this.parent.event();
            } else {
                this.anchorOffsets = [0, 0]
            }

            const f = this.parent.getFidelity();
            const [x, y] = [xy[0] / f, xy[1] / f];
            let [cx, cy, w, h] = this.getScreenPosition();

            cx -= this.anchorPadding;
            cy -= this.anchorPadding;
            w += 2 * this.anchorPadding;
            h += 2 * this.anchorPadding;

            cx += this.anchorOffsets[0]/2.5;
            cy += this.anchorOffsets[1]/2.5;

            const [px, py] = [(cx - x) / w * 100, (cy - y) / h * 100];

            cursor.setShape(w, h, this.radius + this.anchorPadding/2);
            cursor.setTransform(px, py);
        }
    }

    getAnchorOffsetRect() {
        return [
            this.rect[0] + this.anchorOffsets[0],
            this.rect[1] + this.anchorOffsets[1],
            this.rect[2], this.rect[3]
        ];
    }

    onScroll(dxy: number[], evt: WheelEvent): void {
    }

    onZoom(dy: number, xy: number[], evt: WheelEvent): void {
    }

    onMouseEnter(xy: number[], evt: MouseEvent): void {

    }

    onMouseExit(xy: number[], evt: MouseEvent): void {
        this.anchorOffsets = [0, 0];
        this.parent.event();
    }

}