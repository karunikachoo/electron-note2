import {Surface} from "./surface";
import {Utils} from "../utils/Utils";

// NO OBJECT NESTING

export class SurfaceObject {
    id: string = Utils.genId(8);
    parent: Surface;

    rect: number[] = [0, 0, 0, 0];

    children: string[] = [];

    /** Flags */
    fixed: boolean = false;     // no scale
    overlay: boolean = false;

    constructor(parent: Surface) {
        this.parent = parent;
    }

    setRect(x:number, y:number, w:number, h:number) {
        this.rect[0] = x;
        this.rect[1] = y;
        this.rect[2] = w;
        this.rect[3] = h;
    }

    setOffset(x:number, y:number) {
        this.rect[0] = x;
        this.rect[1] = y;
    }
    setWidth(w: number) { this.rect[2] = w; }
    setHeight(h: number) { this.rect[3] = h; }

    getScaledOffsetPoints(offset?:number[], scale?:number) {
        if (!offset) offset = this.parent.getOffset();
        if (!scale) scale = this.parent.scale;

        const [ox, oy] = offset;

        const [x0, y0] = [ox + this.rect[0], oy + this.rect[1]];
        return [
            x0 * scale, y0 * scale,
            (x0 + this.rect[2]) * scale, (y0 + this.rect[3]) * scale
        ];
    }

    getScreenPosition() {
        const [ox, oy] = this.fixed ? [0, 0] : this.parent.getOffset();
        const s = this.fixed ? 1 : this.parent.getScale();
        const f = this.parent.getFidelity();
        return [
            (ox + this.rect[0]) / (s * f),
            (oy + this.rect[1]) / (s * f),
            this.rect[2] / (s * f),
            this.rect[3] / (s * f)
        ];
    }

    draw(ctx: CanvasRenderingContext2D, ox:number, oy:number, s:number, f:number) {
        this.children.forEach(id => {
            const so = this.parent.getObject(id);
            if (so) {
                if (this.parent._checkDraw(so)) so.draw(ctx, ox, oy, s, f);
            }
        })
    }
}